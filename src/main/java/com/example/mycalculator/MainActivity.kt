package com.example.mycalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import java.lang.ArithmeticException

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    fun onClear(view: View){
        val calcField : TextView = findViewById<TextView>(R.id.tvInput)
        calcField.text = ""
    }
    fun onDigit(view: View){
        val calcField : TextView = findViewById<TextView>(R.id.tvInput)
        calcField.append((view as Button).text)
    }

    fun onOperator(view: View){
        val calcField : TextView = findViewById<TextView>(R.id.tvInput)
        if (calcField.text.isEmpty() && (view as Button).text[0] == '-'){
            calcField.append((view as Button).text)
        }
        else if (calcField.text.last().isDigit() && !isOperatorAdded(calcField.text.toString())) {
            calcField.append((view as Button).text)
        }
        else if (calcField.text.length > 1 && (view as Button).text[0] == '-'){
            if (calcField.text.last() == '+'){

                var tmpText = calcField.text.toString()
                tmpText = tmpText.removeSuffix("+") + '-'
                calcField.text = tmpText

            }
            else if (calcField.text.last() == '-' && !calcField.text.contains('*') && !calcField.text.contains('/')){

                var tmpText = calcField.text.toString()
                tmpText = tmpText.removeSuffix("-") + '+'
                calcField.text = tmpText
            }
            else if (calcField.text.last() == '/' ||  calcField.text.last() == '*'){
                calcField.append((view as Button).text)
            }
        }
    }

    fun onDecimal(view: View){
        val calcField : TextView = findViewById<TextView>(R.id.tvInput)
        var tmpText = calcField.text
        if (!calcField.text.contains(".")) {
            if (calcField.text.isEmpty()) {
                calcField.append("0.")
            } else {
                calcField.append(".")
            }
        }
        else if (isOperatorAdded(tmpText.toString())){
            if (tmpText.filter{ it == '.' }.count() < 2) {
                val lastNum = tmpText.last()
                when (lastNum){
                    '/','+', '-', '*' -> calcField.append("0.")
                    else              -> calcField.append(".")
                }
           }
        }
    }
    fun onEqual(view: View){
        val calcField : TextView = findViewById<TextView>(R.id.tvInput)
        if (calcField.text.last().isDigit()){
            var inputText = calcField.text.toString()
            try {
                if (inputText.contains("*")){
                    var splitText = inputText.split("*")
                    var result = (splitText[0].toDouble() * splitText[1].toDouble()).toString()
                    splitText = result.split(".")
                    if (splitText[1].equals("0")){
                        result = splitText[0]
                    }
                    calcField.text = result
                }
                else if  (inputText.contains("+")) {
                    var splitText = inputText.split("+")
                    var result = (splitText[0].toDouble() + splitText[1].toDouble()).toString()
                    splitText = result.split(".")
                    if (splitText[1].equals("0")) {
                        result = splitText[0]
                    }
                    calcField.text = result
                }
                else if (inputText.contains("/")) {
                    var splitText = inputText.split("/")
                    if (splitText[1].toDouble() != 0.0){
                        var result = (splitText[0].toDouble() / splitText[1].toDouble()).toString()
                        splitText = result.split(".")
                        if (splitText[1].equals("0")) {
                            result = splitText[0]
                        }
                        calcField.text = result

                    }
                }
                else if (inputText.contains("-")) {
                    var prefix = ""
                    if (inputText.startsWith('-')){
                        inputText = inputText.drop(1)
                        prefix = "-"
                    }
                    var splitText = inputText.split("-")
                    var firstNumber = splitText[0].toString()
                    if (prefix.isNotEmpty()){
                        firstNumber = prefix + firstNumber
                    }
                    var result = (firstNumber.toDouble() - splitText[1].toDouble()).toString()
                    splitText = result.split(".")
                    if (splitText[1].equals("0")) {
                            result = splitText[0]
                    }
                    calcField.text = result
                }
            }
            catch (e : ArithmeticException){
                e.printStackTrace()
            }
        }

    }
    private fun isOperatorAdded(text: String) : Boolean {
        var tmpText = text
        if (tmpText.startsWith("-")){
            tmpText = tmpText.substring(1)
        }
        return tmpText.contains("/") || tmpText.contains("+") || tmpText.contains("*")
                || tmpText.contains("-")
    }
}